# Refactoring

You've been asked to refactor the function `deterministicPartitionKey` in [`dpk.js`](dpk.js) to make it easier to read and understand without changing its functionality. For this task, you should:

1. Write unit tests to cover the existing functionality and ensure that your refactor doesn't break it. We typically use `jest`, but if you have another library you prefer, feel free to use it.
2. Refactor the function to be as "clean" and "readable" as possible. There are many valid ways to define those words - use your own personal definitions, but be prepared to defend them. Note that we do like to use the latest JS language features when applicable.
3. Write up a brief (~1 paragraph) explanation of why you made the choices you did and why specifically your version is more "readable" than the original.

You will be graded on the exhaustiveness and quality of your unit tests, the depth of your refactor, and the level of insight into your thought process provided by the written explanation.

## Your Explanation Here

The refactoring aims to improve readability by reducing the if/else statement nesting and removing unnecessary double checks in some cases. The functions `convertToString` and `createHash` helps to make clear the purpose of some code parts while reducing the code repetition. The `formatPartitionKey` method extracts the logic that is only needed if the input event contains a partition key and only applies it in that case, returning null otherwise. Combining that with the nullish operator, if necessary, we can generate a hash of the entire input and return it as the partition key. In this second scenario, we don't need to check the type and length of the returned value, since it will always be a string smaller than the maximum size.
