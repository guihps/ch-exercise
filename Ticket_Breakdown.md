# Ticket Breakdown
We are a staffing company whose primary purpose is to book Agents at Shifts posted by Facilities on our platform. We're working on a new feature which will generate reports for our client Facilities containing info on how many hours each Agent worked in a given quarter by summing up every Shift they worked. Currently, this is how the process works:

- Data is saved in the database in the Facilities, Agents, and Shifts tables
- A function `getShiftsByFacility` is called with the Facility's id, returning all Shifts worked that quarter, including some metadata about the Agent assigned to each
- A function `generateReport` is then called with the list of Shifts. It converts them into a PDF which can be submitted by the Facility for compliance.

## You've been asked to work on a ticket. It reads:

**Currently, the id of each Agent on the reports we generate is their internal database id. We'd like to add the ability for Facilities to save their own custom ids for each Agent they work with and use that id when generating reports for them.**


Based on the information given, break this ticket down into 2-5 individual tickets to perform. Provide as much detail for each ticket as you can, including acceptance criteria, time/effort estimates, and implementation details. Feel free to make informed guesses about any unknown details - you can't guess "wrong".


You will be graded on the level of detail in each ticket, the clarity of the execution plan within and between tickets, and the intelligibility of your language. You don't need to be a native English speaker, but please proof-read your work.

## Your Breakdown Here

**Guesses**
We are using a relational database with a migration system;
The users uses a web page to create and update Agents registers;
The custom id is not always numeric;

**Ticket #1:**
Add new field on Agents table for agent custom id
Create a migration to add the field `custom_id` as a unique text field to the table Agents. For the default value, we can use the same value as the database id.
The migration should have 3 steps: Add `custom_id` column to the table; Update all rows to use same value as `id` (converted to string); Add unique constraint to `custom_id` column.

Acceptance criteria:
Table Agents must have the new field `custom_id`
The field `custom_id` must be a text field
The field `custom_id` must have unique constraint

Estimate: 2

**Ticket #2:**
Add custom id field on Agent create and update pages
Update the pages and forms used to create/update Agents, include the Custom Id field and add unique validation for it.

Acceptance criteria:
User can create a new Agent with a custom id
User can update a existing Agent and add a custom id
If the custom id is already used, a message error must be displayed

Estimate: 5

**Ticket #3:**
Update getShiftsByFacility to include custom id in the Agent metadata
Update the query used to find the Shifts to retrieve the `custom_id` field from the Agent table and add that information to the returned objects

Acceptance criteria:
Shifts returned by the function must include Agent custom_id field

Estimate: 2

**Ticket #4:**
Update `generateReport` to use Agent's custom id when generating the report
Change the field used as Agent id in generated report: we must use the field `custom_id` instead of the database `id`

Acceptance criteria:
The generated report must show the custom id in the Agent Id field, not the database id

Estimate: 1

**Execution plan:**
First of all, the Ticket #1 must be completed. This will allow the start of the other tickets.
Then, we can split in two separate and independent flows: allowing the users to save the agents custom ids (Ticket #2) and including the custom id on the generated report (first Ticket #3 and then Ticket #4)
