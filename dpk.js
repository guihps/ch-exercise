const crypto = require("crypto");

exports.deterministicPartitionKey = (event) => {
  const TRIVIAL_PARTITION_KEY = "0";

  if (!event) return TRIVIAL_PARTITION_KEY;

  return formatPartitionKey(event.partitionKey) ?? createHash(JSON.stringify(event));
};

function createHash(data) {
  return crypto.createHash("sha3-512").update(data).digest("hex");
}

function formatPartitionKey(partitionKey) {
  const MAX_PARTITION_KEY_LENGTH = 256;

  if (!partitionKey) return null;

  let formattedKey = convertToString(partitionKey);

  if (formattedKey.length > MAX_PARTITION_KEY_LENGTH) {
    formattedKey = createHash(formattedKey)
  }

  return formattedKey;
}

function convertToString(data) {
  return typeof(data) === "string" ? data : JSON.stringify(data)
}