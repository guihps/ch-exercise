const { deterministicPartitionKey } = require("./dpk");

describe("deterministicPartitionKey", () => {
  it("Returns the literal '0' when given no input", () => {
    const trivialKey = deterministicPartitionKey();
    expect(trivialKey).toBe("0");
  });
  
  it("Returns the partition key if it is present on event", () => {
    const expectedKey = "PARTITION_KEY";
	const inputEvent = { "partitionKey": expectedKey, "foo": "bar"}
	const returnedKey = deterministicPartitionKey(inputEvent);
    expect(returnedKey).toBe(expectedKey);
  });
  
  it("Returns the event hash if partition key is not present on event", () => {
    const expectedKey = "a419a15de4a65c3dba49c38b4485cd4dce1dde4d18f5b965d90f0649bef54252ec4e76dbcaa603708e8e8ebbe848ba484e81e23b7823808b5b8e5f4222d122e8";
	const inputEvent = { "foo": "bar" }
	const returnedKey = deterministicPartitionKey(inputEvent);
    expect(returnedKey).toBe(expectedKey);
  });
  
  it("Returns the event hash if partition key is present on event, but is empty", () => {
    const expectedKey = "29239948d0b27f69014b71050f9c2983d8efbb17c9cecb4f270b28386bc6456d311fb2b46abcfaf5e610c21f5845c24d47e3791805c4434724ee91ca12f33191";
	const inputEvent = { "partitionKey": "", "foo": "bar" }
	const returnedKey = deterministicPartitionKey(inputEvent);
    expect(returnedKey).toBe(expectedKey);
  });
  
  it("Returns the partition key hash if existing partition key is too long", () => {
    const eventPartitionKey = "KbBppQmZ90pOmKZMGS6E4ez98ktLoYME518oQhJUMfJNseKmLR4bOU3NJ7twfKhE3o6k46W03Lo2mNGbMfQ1CBock3equJJtkzbCdrpiXrIEr4eAH9VOr7mxCsgqAED431bXe89XnHRMHFOPmhKIiOUKQ3ax0NV4lFiJlZqXTDTJfDcja3vHWIJnjP5K3pv4vus5XkmMsp7FBFDSPJWyJdos1CUzlTZlQzLiv0n6qnBkdLl2vE5USsoRBPcXuiYQ8DGAyBzdgqgyBovglggd8tNtujkfIBDn3GEhfT7UIjRB"
	const expectedKey = "c61508649b7d197aeb90db80cd1ff806fa793dec3da6e75189194c11ad3787b9469ab8c409c2497a3b53095f5f915049f7f9f8245a0aa84590a8443f7bf20038";
	const inputEvent = { "partitionKey": eventPartitionKey, "foo": "bar"}
	const returnedKey = deterministicPartitionKey(inputEvent);
    expect(returnedKey).toBe(expectedKey);
  });
  
  it("Returns the partition key as string if existing partition key is a number", () => {
    const eventPartitionKey = 12345
	const expectedKey = "12345";
	const inputEvent = { "partitionKey": eventPartitionKey, "foo": "bar"}
	const returnedKey = deterministicPartitionKey(inputEvent);
    expect(returnedKey).toBe(expectedKey);
  });
  
  it("Returns the partition key as string if existing partition key is an object", () => {
    const eventPartitionKey = {"key": "partition_key"}
	const expectedKey = "{\"key\":\"partition_key\"}";
	const inputEvent = { "partitionKey": eventPartitionKey, "foo": "bar"}
	const returnedKey = deterministicPartitionKey(inputEvent);
    expect(returnedKey).toBe(expectedKey);
  });
  
  it("Returns the input hash if the input is not a object", () => {
    const expectedKey = "87ea696b2a004579de7dcf24442551749920fd7094df2002272585bdfd489dc98585f85f115425ade87c152f387cc3d69cd1fc4371d56daf5a4d2d6db8f6f087";
	const inputEvent = "not_an_object"
	const returnedKey = deterministicPartitionKey(inputEvent);
    expect(returnedKey).toBe(expectedKey);
  });
});
